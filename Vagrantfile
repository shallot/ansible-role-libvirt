# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version '>= 2.0.0'

# https://www.vagrantup.com/docs/vagrantfile/version.html
Vagrant.configure(2) do |vagrant|

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.synced_folder('.', '/vagrant', disabled: true)

  # https://www.vagrantup.com/docs/multi-machine/
  vagrant.vm.define('debian-buster.test') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'debian/buster64'
    config.vm.hostname = 'debian-buster.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = '2.0'
      ansible.playbook = File.join(__dir__, 'provision-localhost.yml')
    end

  end

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.box_check_update = false
  vagrant.vm.post_up_message = nil

end
